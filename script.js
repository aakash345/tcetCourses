const api_url = "https://tcetcourses.herokuapp.com/courses"

function loadData(records = []) {
	var table_data = "";
	for(let i=0; i<records.length; i++) {
		table_data += `<tr>`;
		table_data += `<td>${records[i].course_name}</td>`;
		table_data += `<td>${records[i].course_author}</td>`;
		table_data += `<td>${records[i].course_duration}</td>`;
		table_data += `<td>${records[i].course_fee}</td>`;
		table_data += `<td>`;
		table_data += `<a href="edit.html?id=${records[i]._id}"><button class="btn btn-primary">Edit</button></a>`;
		table_data += '&nbsp;&nbsp;';
		table_data += `<button class="btn btn-danger" onclick=deleteData('${records[i]._id}')>Delete</button>`;
		table_data += `</td>`;
		table_data += `</tr>`;
	}
	//console.log(table_data);
	document.getElementById("tbody").innerHTML = table_data;
}

function getData() {
	fetch(api_url)
	.then((response) => response.json())
	.then((data) => { 
		console.table(data); 
		loadData(data);
	});
}

function getDataById(id) {
	fetch(`${api_url}/${id}`)
	.then((response) => response.json())
	.then((data) => { 
	
		console.log(data);
		document.getElementById("id").value = data._id;
		document.getElementById("coursename").value = data.course_name;
		document.getElementById("author").value = data.course_author;
		document.getElementById("duration").value = data.course_duration;
		document.getElementById("fees").value = data.course_fee;
	})
}


function postData() {
	var coursename = document.getElementById("coursename").value ;
	var author = document.getElementById("author").value;
	var duration = document.getElementById("duration").value;
	var fees = document.getElementById("fees").value;
	
	data = {course_name: coursename, course_author: author, course_duration: duration, course_fee: fees};
	
	fetch(api_url, {
		method: "POST",
		headers: {
		  'Accept': 'application/json',
		  'Content-Type': 'application/json'
		},
		body: JSON.stringify(data)
	})
	.then((response) => response.json())
	.then((data) => { 
		console.log(data); 
		window.location.href = "index.html";
	})
}	


function putData() {
	
	var _id = document.getElementById("id").value;
	var coursename = document.getElementById("coursename").value ;
	var author = document.getElementById("author").value;
	var duration = document.getElementById("duration").value;
	var fees = document.getElementById("fees").value;
	
	data = {_id: _id, course_name: coursename, course_author: author, course_duration: duration, course_fee: fees};
	
	fetch(api_url, {
		method: "PUT",
		headers: {
		  'Accept': 'application/json',
		  'Content-Type': 'application/json'
		},
		body: JSON.stringify(data)
	})
	.then((response) => response.json())
	.then((data) => { 
		console.table(data);
		window.location.href = "index.html";
	})
}


function deleteData(id) {
	user_input = confirm("Are you sure you want to delete this record?");
	if(user_input) {
		fetch(api_url, {
			method: "DELETE",
			headers: {
			  'Accept': 'application/json',
			  'Content-Type': 'application/json'
			},
			body: JSON.stringify({"_id": id})
		})
		.then((response) => response.json())
		.then((data) => { 
			console.log(data); 
			window.location.reload();
		})
	}
}
